package version

import (
	"encoding/json"
	"fmt"
	"regexp"
	"strconv"
)

// Versione struct che indica la versione
type Versione struct {
	Major    int
	Minor    int
	Revision int
	Build    string
	//NoteRilascio []string
}

// Assert implementation
var _ fmt.Stringer = Versione{}
var _ json.Marshaler = Versione{}
var _ json.Unmarshaler = &Versione{}

// IsEmpty è true se major, minor e revision sono a 0
func (v *Versione) IsEmpty() bool {
	return v.Major == 0 && v.Minor == 0 && v.Revision == 0
}

// Implement Stringer interface
func (v Versione) String() string {
	return v.ToString()
}

// Implement Marshaler interface
func (v Versione) MarshalJSON() ([]byte, error) {
	val := []byte("\"" + v.ToString() + "\"")
	return val, nil
}

// Implement Unmarshaler interface
func (v *Versione) UnmarshalJSON(js []byte) error {
	strJs := string(js)
	if strJs == "null" {
		return nil
	}

	rx := regexp.MustCompile(`^\"|\"$`)
	filtered := rx.ReplaceAllString(strJs, "")

	err := v.FromString(filtered)
	return err
}

// Ritorna una Versione a partire dalla stringa s. Se s non è una stringa valida,
// la Versione ritornata sarà equivalente a 0.0.0 (condizione verificabile tramite
// il metodo IsEmpty)
func FromString(s string) (v Versione) {
	v.FromString(s)
	return v
}

// ToString converte una versione in stringa
func (v *Versione) ToString() string {
	ver := strconv.Itoa(v.Major) + "." + strconv.Itoa(v.Minor) + "." + strconv.Itoa(v.Revision)
	if len(v.Build) > 0 {
		ver += "+" + v.Build
	}
	return ver
}

// FromString converte una stringa in versione
func (v *Versione) FromString(value string) error {
	if len(value) == 0 {
		return fmt.Errorf("la stringa di versione non può essere vuota")
	}

	rx := regexp.MustCompile(`([0-9]+)\.([0-9]+)\.([0-9]+)(?:\+(\S+))?`)
	tokens := rx.FindStringSubmatch(value)

	if tokens == nil {
		return fmt.Errorf("la stringa non contiene una versione valida")
	}

	var err error
	v.Major, err = strconv.Atoi(tokens[1])
	if err != nil {
		return fmt.Errorf("errore nella regexp: major non è un numero")
	}
	v.Minor, err = strconv.Atoi(tokens[2])
	if err != nil {
		return fmt.Errorf("errore nella regexp: minor non è un numero")
	}
	v.Revision, err = strconv.Atoi(tokens[3])
	if err != nil {
		return fmt.Errorf("errore nella regexp: revision non è un numero")
	}

	v.Build = tokens[4]

	// // Rimuove la "v" iniziale, se presente
	// if value[0] == 'v' {
	// 	value = value[1:]
	// }

	// // strip build metadata
	// splitplus := strings.Split(value, "+")

	// if len(splitplus) > 2 {
	// 	panic("troppi + nella stringa di versione " + value)
	// }

	// if len(splitplus) > 1 {
	// 	v.Build = splitplus[1]
	// }

	// parti := strings.Split(splitplus[0], ".")

	// var err error
	// v.Major, err = strconv.Atoi(parti[0])
	// if err != nil {
	// 	panic(err)
	// }
	// v.Minor, err = strconv.Atoi(parti[1])
	// if err != nil {
	// 	panic(err)
	// }
	// v.Revision, err = strconv.Atoi(parti[2])
	// if err != nil { //NON OBBLIGATORIO?
	// 	v.Revision = 0
	// 	//panic(err)
	// }
	return nil
}

// IsGreaterThan se maggiore
func (v *Versione) IsGreaterThan(of Versione) bool {
	if v.Major == of.Major {
		if v.Minor == of.Minor {
			return v.Revision > of.Revision
		}
		return v.Minor > of.Minor
	}
	return v.Major > of.Major
}

// IsLessThan se minore
func (v *Versione) IsLessThan(of Versione) bool {
	if v.Major == of.Major {
		if v.Minor == of.Minor {
			return v.Revision < of.Revision
		}
		return v.Minor < of.Minor
	}
	return v.Major < of.Major
}

// IsEqualTo se uguale
func (v *Versione) IsEqualTo(of Versione) bool {
	return v.Major == of.Major && v.Minor == of.Minor && v.Revision == of.Revision
}
